#!/usr/bin/sh

echo "Testing Git command"
curl --data "vcstype=git&vcsurl=git@bitbucket.org:shackra/st.-vincent-ferrer.git" http://localhost:1455
echo "Testing Mercurial command"
curl --data "vcstype=mercurial&vcsurl=ssh://hg@bitbucket.org/shackra/puntoarchivos" http://localhost:1455
