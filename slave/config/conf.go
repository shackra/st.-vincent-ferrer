// Copyright (C) 2015  Jorge Araya Navarro

// This file is part of St. Vincent Ferrer.

// St. Vincent Ferrer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// St. Vincent Ferrer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with St. Vincent Ferrer. If not, see <http://www.gnu.org/licenses/>.

package config

import (
	"os/user"
	"path/filepath"
	"regexp"
)

var User, _ = user.Current()
var DEFAULT_WORKSPACE_PATH = filepath.Join(User.HomeDir, "stvincent_workspace")

func GetWorkspace() string {
	return DEFAULT_WORKSPACE_PATH
}

func GetRepoDir(repourl string) string {
	// retorna la ubicación local del repositorio en el espacio de trabajo
	// según su URL
	re := regexp.MustCompile("[.]git$")
	base := filepath.Base(repourl)
	base = re.ReplaceAllString(base, "")
	repodir := filepath.Join(GetWorkspace(), base)
	return repodir
}
