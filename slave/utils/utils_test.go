// Copyright (C) 2015  Jorge Araya Navarro

// This file is part of St. Vincent Ferrer.

// St. Vincent Ferrer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// St. Vincent Ferrer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with St. Vincent Ferrer. If not, see <http://www.gnu.org/licenses/>.

package utils

import (
	"."
	"testing"
)

func TestReadConf(t *testing.T) {
	conf, err := utils.ReadYAML("../../")
	if err != nil {
		t.Error("There was an error", err)
	} else {
		if conf.Steps[0]["script"]["name"] != "build command" {
			t.Error("conf.Steps[0][\"script\"][\"name\"] != \"build command\"")
		}
	}
}
