// Copyright (C) 2015  Jorge Araya Navarro

// This file is part of St. Vincent Ferrer.

// St. Vincent Ferrer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// St. Vincent Ferrer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with St. Vincent Ferrer. If not, see <http://www.gnu.org/licenses/>.

package utils

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"path/filepath"
)

type StVincentProjConf struct {
	BeforeSteps []map[string]map[string]string `yaml:"before-steps"`
	Steps       []map[string]map[string]string `yaml:"steps"`
	AfterSteps  []map[string]map[string]string `yaml:"after-steps"`
}

type MyWriter struct {
}

func (m MyWriter) Write(p []byte) (n int, err error) {
	log.Println("MyWriter:", string(p[:]))
	return len(p), nil
}

func ReadYAML(projectrootpath string) (StVincentProjConf, error) {
	// a partir del archivo de configuración que debe estar en la raíz del
	// proyecto, se extrae la configuración del proyecto a un diccionario
	// que sera devuelto para su uso posterior.
	// `~` no es expandido por la aplicación.

	yamlpath := filepath.Join(projectrootpath, "stvincent.yaml")
	yamlcontent, err := ioutil.ReadFile(yamlpath)

	if err != nil {
		log.Println(err)
		return StVincentProjConf{}, err
	}

	var conf StVincentProjConf
	err = yaml.Unmarshal(yamlcontent, &conf)

	if err != nil {
		log.Panicln(err)
		return StVincentProjConf{}, err
	}

	return conf, nil
}
