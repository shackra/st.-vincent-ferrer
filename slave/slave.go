// Copyright (C) 2015  Jorge Araya Navarro

// This file is part of St. Vincent Ferrer.

// St. Vincent Ferrer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// St. Vincent Ferrer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with St. Vincent Ferrer. If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"./config"
	"./utils"
	"log"
	"net/http"
	"net/url"
	"os/exec"
)

func CloneRepo(repotype, repourl string) ([]string, string, error) {
	// clona un repositorio a la maquina local

	// especificamos el espacio de trabajo donde colocar el clon del
	// repositorio
	repodir := config.GetRepoDir(repourl)
	cmd := &exec.Cmd{}

	// ejecutamos el comando correspondiente según el tipo de software para
	// control de versiones
	if repotype == "mercurial" {
		cmd = exec.Command("hg", "-v", "--time", "clone", repourl, repodir)
	} else if repotype == "git" {
		cmd = exec.Command("git", "clone", repourl, repodir)
	}

	mw := utils.MyWriter{}
	cmd.Stdout = mw

	err := cmd.Run()

	return cmd.Args, repodir, err
}

func PullUpdate(repotype, repodir string) error {
	// si el repositorio ya existe, lo actualiza a su ultima versión con
	// relación al repositorio de código fuente remoto.

	cmd := &exec.Cmd{}
	mw := utils.MyWriter{}

	// ejecutamos el comando correspondiente según el tipo de software para
	// control de versiones
	if repotype == "mercurial" {
		cmd = exec.Command("hg", "-v", "--time", "pull", "--update")
	} else if repotype == "git" {
		cmd = exec.Command("git", "pull", "--no-edit")
	}

	cmd.Stdout = mw
	cmd.Dir = repodir
	err := cmd.Run()
	return err
}

func ProcessCommands(w http.ResponseWriter, r *http.Request) {
	// procesa los valores de formulario
	r.ParseForm()
	w.Write([]byte{})

	// ayuda a diferenciar el tipo de control de versiones
	repotype := r.FormValue("vcstype")
	// indica la URL del repositorio para clonarlo posteriormente
	repourlstr := r.FormValue("vcsurl")

	// procesa la URL para asignarle las credenciales
	repourl, err := url.Parse(repourlstr)

	if err != nil {
		log.Fatal("Error parsing repository URL:", err)
	}

	var args []string
	var dir string

	// se clona el repositorio
	// `dir` es la ubicación absoluta del repositorio en la carpeta de
	// trabajo de la aplicación
	args, dir, err = CloneRepo(repotype, repourl.String())

	if err != nil {
		// si el repositorio ya existe, se trata de actualizar
		err = PullUpdate(repotype, dir)
		if err != nil {
			log.Println("Error while attempting to clone or update existing repository:", err, args)
		} else {
			log.Println("Pull changes for", dir, "finished successfully!")
		}
	} else {
		log.Println("Cloning", repourl.String(), "finished successfully!")
		log.Println("Clone in", dir)
	}

	var repoconf utils.StVincentProjConf
	// leemos la configuración del proyecto
	repoconf, err = utils.ReadYAML(dir)
	if err != nil {
		log.Println(err)
	} else {
		log.Println(repoconf)
	}
}

func main() {
	// mapea un camino con un metodo
	http.HandleFunc("/", ProcessCommands)

	log.Println("Listening on :1455")
	http.ListenAndServe(":1455", nil)
}
