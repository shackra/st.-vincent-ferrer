A cross-platform Continuous integration software written in [Go](http://golang.org).

[![wercker status](https://app.wercker.com/status/c274bd0de9915b86d484b6027777f738/m "wercker status")](https://app.wercker.com/project/bykey/c274bd0de9915b86d484b6027777f738)

## Why such name?

St. Vincent Ferrer is the patron saint of builders because of his fame for "building up" and strengthening the Church: through his preaching, missionary work, in his teachings, as confessor and adviser.

![](http://i.imgur.com/xqbypN6.png)

So, now you know! ;)

## Why you created yet another CI software?

Well, because I love-hate Jenkins-CI. See, I work as freelancer writing applications with Python developing on GNU/Linux and targeting the Windows operating system as the final destination of my work.

I've been using Jenkins-CI to build my own projects, and at certain point of my freelance career I decided that was time to start using Jenkins-CI to build my clients' projects too. I wanted to make my Windows image in Rackspace a slave node to use with Jenkins-CI, but I ran in a lot of troubles... half of those problems would get solve if I deploy Jenkins in a server on the Internet instead of keeping it local and behind my home router, but I don't have good enough money to pay month-a-month for a VPS. A quarter of the problem was Jenkins support for Python projects too.

I'm not saying that Jenkins-CI is bad software or anything, what I'm saying that Jenkins-CI wasn't for me (because I'm too dumb, maybe?).

Later I discovered [Travis-CI](https://travis-ci.org/), but it was just about unit testing. Nothing wrong with that since I love unit testing. But I needed to build my clients' projects too and for running on Windows.

Then I discovered [Wercker](http://wercker.com/), it is a pretty awesome project. I love the fact they support Bitbucket repositories. But still, no Windows boxes and no support for [Mercurial](http://mercurial.selenic.com/) repositories.

Then, I decided to simply give up all these awesome services and add a Python script to any project with instructions to test, build and upload artifacts to Rackspace Cloud Files, script that I will run manually in my cloud server fresh recreated from my custom Windows image. The process of writing such script and running it manually is tedious but it was better than anything.

But tedious things are still tedious, and I want to save me the stress of going through that process, I want to automate it!!. I saw this particularly important when working for many clients at the same time.

So, this project was born for the sake of my needs as developer.

## License

see the `LICENSE` file!
